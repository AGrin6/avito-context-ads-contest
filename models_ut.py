import unittest
import models
from constants import X

class TestProcessSubarray(unittest.TestCase):
  def test_all(self):
    subarray_code = (X, 0)
    precomputed_dict = {(0, 0): 1}
    model_shape = (2, 2)
    print('--', models.process_subarray(subarray_code, precomputed_dict, model_shape))


class TestNearestNeighborsModel(unittest.TestCase):
  def test_all(self):
    list_of_samples = [(0, 0), (0, 0), (1, 0)]
    list_of_labels = [1, 0, 0]
    testing_model = models.NearestNeighborsModel((2, 2), 0.0001, 2, (1, 2))
    testing_model.fit(list_of_samples, list_of_labels)
    print(testing_model.predict((0, 0)))
    print(testing_model.predict((1, 0)))
    print(testing_model.predict((0, 1)))
    print(testing_model.predict((1, 1)))
    print('')

    list_of_samples = [(0, 0), (0, 0), (1, 0), (0, 1)]
    list_of_labels = [0, 0, 0, 1]
    testing_model = models.NearestNeighborsModel((2, 2), 0.0001, 2, (1, 2))
    testing_model.fit(list_of_samples, list_of_labels)
    print(testing_model.predict((1, 1)))


if __name__ == '__main__':
  unittest.main()

