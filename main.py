# TODO def get_prediction(sample)
# TODO right subsplit to normalizing subset

import os
import sys
import math
import time
import matplotlib.pyplot as plotter
from utils import load_train_data
from utils import load_validation_data
from utils import load_test_data
from utils import load_timestamp_cache
from utils import load_timestamp_labels_cache
from utils import get_list_of_values
from utils import get_model_shape
from models import NeighborhoodProbabilityModel
from wrappers import SimpleVotingRegression
from wrappers import to_index
from hyperparams import LEARNING_MODEL
from hyperparams import ORIGINAL_CELL_MULTIPLIER
from hyperparams import LISTS_OF_FEATURES
from hyperparams import FEATURE_MULTIPLIERS
from hyperparams import NUMBER_OF_PRICE_BINS
from hyperparams import MAX_PRICE
from hyperparams import MODEL_TYPES
from hyperparams import BIN_NUMBERS
from hyperparams import MAX_VALUES
from hyperparams import COEFFICIENTS

CORRECTING_FUNCTION = lambda(x): 0.2 * (x + 0.02)
BOUNDARY_SHIFT = 0.0001
NUMBER_OF_TRAIN_SAMPLES = 10 ** 6
# NUMBER_OF_BINS = 3000
NUMBER_OF_BINS = 1000

def load_ad_to_category_data():
  result = dict()
  with open('./data/AdsInfo.tsv') as input_stream:
    input_stream.readline()
    for line in input_stream:
      ad_id = int(line.split('\t')[0])
      category_id_str = line.split('\t')[2]
      if category_id_str != '':
        category_id = int(category_id_str)
      else:
        # category_id = ''
        category_id = -1
      result[ad_id] = category_id

  return result


def to_sample_train(line):
  position = int(line[:-1].split('\t')[2])
  hist_ctr = float(line[:-1].split('\t')[5])
  return (position, hist_ctr)


def to_sample_test(line):
  position = int(line[:-1].split('\t')[3])
  hist_ctr = float(line[:-1].split('\t')[5])
  return (position, hist_ctr)


def load_data(number_of_samples, skipped_samples=0, is_full_dataset=False):
  sys.stdout.write('Loading ad categories...')
  sys.stdout.flush()
  ad_to_category_dict = load_ad_to_category_data()
  sys.stdout.write(' completed\n')
  sys.stdout.flush()

  list_of_samples = []
  list_of_labels = []
  id_counter = 0
  if is_full_dataset:
    file_name = './data/trainSearchStream.tsv'
  else:
    file_name = './data/validation/train.tsv'
  with open(file_name) as input_stream:
    input_stream.readline()

    for line in input_stream:
      prediction_id = int(line[:-1].split('\t')[0])
      if prediction_id / 1000000 > id_counter:
        print(id_counter)
        id_counter += 1
      if prediction_id > skipped_samples:
        break

    for line in input_stream:
      prediction_id = int(line[:-1].split('\t')[0])
      if prediction_id / 1000000 > id_counter:
        print(id_counter)
        id_counter += 1
      if prediction_id > number_of_samples + skipped_samples:
        break
      if line[:-1].split('\t')[-1] != '':
        ad_id = int(line[:-1].split('\t')[1])
        position = int(line[:-1].split('\t')[2])
        histCTR = float(line[:-1].split('\t')[-2])
        is_click = float(line[:-1].split('\t')[-1])

        ad_category = ad_to_category_dict[ad_id]

        list_of_samples.append((position, histCTR, ad_category))
        list_of_labels.append(is_click)
  print('Data loading completed\n')
  return list_of_samples, list_of_labels


def generate_normalizing_list(list_of_likelyhoods, list_of_labels, number_of_bins):
  sys.stdout.write('Generating normalizing function...')
  sys.stdout.flush()
  positive_bins = [0] * number_of_bins
  negative_bins = [0] * number_of_bins
  special_positive_bin = 0
  special_negative_bin = 0
  for sample, label in zip(list_of_likelyhoods, list_of_labels):
    histCTR = sample
    if histCTR <= 0.1 ** 5:
      if label == 0:
        special_negative_bin += 1
      else:
        special_positive_bin += 1
    else:
      bin_number = min(int(histCTR * number_of_bins), number_of_bins - 1)
      try:
        if label == 0:
          negative_bins[bin_number] += 1
        else:
          positive_bins[bin_number] += 1
      except:
        print(sample[1], bin_number)
        raise

  # print(positive_bins[:10])
  # print(negative_bins[:10])
  # print(special_positive_bin, special_negative_bin)

  special_probability_bin = (float(special_positive_bin) /
                                         (special_positive_bin + special_negative_bin))

  probability_bins = []
  for positive_value, negative_value in zip(positive_bins, negative_bins):
    try:
      new_probability = float(positive_value) / (positive_value + negative_value)
    except:
      new_probability = 0.0
    probability_bins.append(new_probability)

  sys.stdout.write(' completed\n')
  sys.stdout.flush()

  return probability_bins, special_probability_bin


def generate_predictions():
  pass


def write_predictions(predictions):
  output_id = 0
  while os.path.exists('./data/output/' + str(output_id) + '.csv'):
    output_id += 1
  with open('./data/output/' + str(output_id) + '.csv', 'w') as output_stream:
    output_stream.write('ID,IsClick\n')
    for prediction_id, prediction_value in predictions:
      output_stream.write(str(prediction_id) + ',' + str(prediction_value) + '\n')


if __name__ == '__main__':
  time_start = time.time()
  print(LEARNING_MODEL.__name__, ORIGINAL_CELL_MULTIPLIER)
  print(LISTS_OF_FEATURES)
  print(FEATURE_MULTIPLIERS)
  print(BIN_NUMBERS)
  print(MAX_VALUES)
  # print('Boundary shift: {}'.format(BOUNDARY_SHIFT))
  # if 'HistCTR' in LIST_OF_FEATURES:
  #   print('HistCTR bins: {}'.format(NUMBER_OF_BINS))
  # if 'Price' in LIST_OF_FEATURES:
  #   print('Price bins: {}, Price max: {}'.format(NUMBER_OF_PRICE_BINS, MAX_PRICE))

  # list_of_used_features = LISTS_OF_FEATURES[0]
  list_of_used_features = set()
  for list_of_features in LISTS_OF_FEATURES:
    list_of_used_features = list_of_used_features.union(set(list_of_features))
  list_of_used_features = list(list_of_used_features)
  train_feature_values = [load_timestamp_cache(feature_name, 2)
                          for feature_name in list_of_used_features]
  train_labels = load_timestamp_labels_cache(2)
  # train_samples, train_labels = load_train_data(LIST_OF_FEATURES)

  model_shapes = []
  for list_of_features, bin_number in zip(LISTS_OF_FEATURES, BIN_NUMBERS):
    model_shapes.append(get_model_shape(list_of_features, bin_number))

  if LEARNING_MODEL.__name__ == 'NeighborhoodProbabilityModel':
    MODEL_ARGS = {'model_shape': model_shape, 'boundary_threshold': BOUNDARY_SHIFT,
                  'neighborhood_radius': None, 'mode': 'sparse',
                  'original_cell_multiplier': ORIGINAL_CELL_MULTIPLIER,
                  'feature_multipliers': FEATURE_MULTIPLIERS}
  elif LEARNING_MODEL.__name__ == 'NearestNeighborsModel':
    MODEL_ARGS = {'model_shape': model_shape, 'boundary_threshold': BOUNDARY_SHIFT,
                  'number_of_neighbors': NUMBER_OF_NEIGHBORS,
                  'feature_distances': range(1, len(model_shape) + 1, 1)}
  elif LEARNING_MODEL.__name__ == 'SimpleVotingRegression':
    MODEL_ARGS = {'list_of_models': [
                     MODEL_TYPES[0](
                          **{'model_shape': model_shapes[0], 'boundary_threshold': BOUNDARY_SHIFT,
                             'neighborhood_radius': None, 'mode': 'sparse',
                             'original_cell_multiplier': ORIGINAL_CELL_MULTIPLIER,
                             'feature_multipliers': FEATURE_MULTIPLIERS}),
                     MODEL_TYPES[1](
                          **{'model_shape': model_shapes[1], 'boundary_threshold': BOUNDARY_SHIFT,
                             'neighborhood_radius': None, 'mode': 'sparse',
                             'original_cell_multiplier': ORIGINAL_CELL_MULTIPLIER,
                             'feature_multipliers': FEATURE_MULTIPLIERS})
                                    ],
                  'features_lists': LISTS_OF_FEATURES,
                  'bin_numbers': BIN_NUMBERS,
                  'max_values': MAX_VALUES,
                  'coefficients': COEFFICIENTS,
                  'all_features': list_of_used_features}
  else:
    raise ValueError


  print(MODEL_ARGS)

  train_model = LEARNING_MODEL(**MODEL_ARGS)

  train_model.fit(zip(*train_feature_values), train_labels)

  validation_feature_values = [load_timestamp_cache(feature_name, 1)
                               for feature_name in list_of_used_features]
  validation_labels = load_timestamp_labels_cache(1)

  total_loss = 0
  samples_counter = 0
  for sample, label in zip(zip(*validation_feature_values), validation_labels):
    prediction = train_model.predict(sample)
    total_loss += label * math.log(prediction) + (1 - label) * math.log(1 - prediction)
    if samples_counter % (len(validation_labels) / 100) == 0:
      sys.stdout.write('\r')
      sys.stdout.write(str(samples_counter / (len(validation_labels) / 100)))
      sys.stdout.flush()
    samples_counter += 1
  sys.stdout.write('\r')
  sys.stdout.flush()
  print('--- ', -total_loss / len(validation_labels), ' ---')

  print('Total validation time: {}'.format(time.time() - time_start))
  # sys.exit()
  print('Predicting started')
  test_model = LEARNING_MODEL(**MODEL_ARGS)

  test_model.fit(zip(*validation_feature_values), validation_labels)


  test_samples, test_ids = load_test_data(list_of_used_features) # need to load only ids here
  test_feature_values = [load_timestamp_cache(feature_name, 0)
                         for feature_name in list_of_used_features]

  predictions = []
  samples_counter = 0
  # for sample_id, sample in zip(test_ids, test_samples):
  for sample_id, sample in zip(test_ids, zip(*test_feature_values)):
    prediction = test_model.predict(sample)
    predictions.append((sample_id, prediction))
    samples_counter += 1
    if samples_counter % 100000 == 0:
      sys.stdout.write('\r')
      sys.stdout.write(str(samples_counter))
      sys.stdout.flush()
  print('')

  # sys.exit()
  write_predictions(predictions)

