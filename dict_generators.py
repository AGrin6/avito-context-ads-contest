import os
import sys
import cPickle

NONE = -1

# AdsInfo dicts

def generate_search_to_category_dict():
  search_to_category_file_name = './data/search_to_category_dict_cached.pkl'
  if os.path.exists(search_to_category_file_name):
    sys.stdout.write('Loading from search_to_category_dict cache...')
    sys.stdout.flush()
    with open(search_to_category_file_name, 'rb') as input_stream:
      search_to_category_dict = cPickle.load(input_stream)
    sys.stdout.write(' completed\n')
    sys.stdout.flush()
  else:
    print('Generating search_to_category dict')
    search_to_category_dict = dict()
    with open('./data/SearchInfo.tsv') as input_stream:
      input_stream.readline()
      counter = 1
      for line in input_stream:
        if counter % 10 ** 6 == 0:
          print(counter)
        search_id = int(line[:-1].split('\t')[0])
        category_id_str = line[:-1].split('\t')[7]
        if category_id_str == '':
          search_to_category_dict[search_id] = NONE
        else:
          category_id = int(category_id_str)
          search_to_category_dict[search_id] = category_id
        counter += 1
    sys.stdout.write('Dumping search_to_category_dict to cache...')
    sys.stdout.flush()
    with open(search_to_category_file_name, 'wb') as output_stream:
      cPickle.dump(search_to_category_dict, output_stream)
    sys.stdout.write(' completed\n')
    sys.stdout.flush()

  return search_to_category_dict


# SearchInfo dicts

def generate_search_to_user_dict():
  cache_file_name = './data/search_to_user_dict_cached.pkl'
  if os.path.exists(cache_file_name):
    sys.stdout.write('Loading from search_to_user dict cache...')
    sys.stdout.flush()
    with open(cache_file_name, 'rb') as input_stream:
      result = cPickle.load(input_stream)
    sys.stdout.write(' completed\n')
    sys.stdout.flush()
  else:
    print('Generating search_to_user dict')
    result = dict()
    with open('./data/SearchInfo.tsv') as input_stream:
      input_stream.readline()
      counter = 1
      for line in input_stream:
        if counter % 10 ** 6 == 0:
          print(counter)
        search_id = int(line[:-1].split('\t')[0])
        # category_id_str = line[:-1].split('\t')[7]
        # if category_id_str == '':
        #   result[search_id] = NONE
        # else:
        #   category_id = int(category_id_str)
        #   result[search_id] = category_id
        user_id = int(line[:-1].split('\t')[3])
        result[search_id] = user_id
        counter += 1
    sys.stdout.write('Dumping search_to_user dict to cache...')
    sys.stdout.flush()
    with open(cache_file_name, 'wb') as output_stream:
      cPickle.dump(result, output_stream)
    sys.stdout.write(' completed\n')
    sys.stdout.flush()

  return result


def generate_user_to_agent_os_dict():
  cache_file_name = './data/user_to_agent_os_dict_cached.pkl'
  if os.path.exists(cache_file_name):
    sys.stdout.write('Loading from user_to_agent_os dict cache...')
    sys.stdout.flush()
    with open(cache_file_name, 'rb') as input_stream:
      result = cPickle.load(input_stream)
    sys.stdout.write(' completed\n')
    sys.stdout.flush()
  else:
    print('Generating user_to_agent_os dict')
    result = dict()
    with open('./data/UserInfo.tsv') as input_stream:
      input_stream.readline()
      counter = 1
      for line in input_stream:
        if counter % 10 ** 6 == 0:
          print(counter)
        user_id = int(line[:-1].split('\t')[0])
        user_agent_os_id = int(line[:-1].split('\t')[2])
        result[user_id] = user_agent_os_id
        counter += 1

    sys.stdout.write('Dumping user_to_agent_os dict to cache...')
    sys.stdout.flush()
    with open(cache_file_name, 'wb') as output_stream:
      cPickle.dump(result, output_stream)
    sys.stdout.write(' completed\n')
    sys.stdout.flush()

  return result


def generate_user_to_device_dict():
  cache_file_name = './data/user_to_device_dict_cached.pkl'
  if os.path.exists(cache_file_name):
    sys.stdout.write('Loading from user_to_device dict cache...')
    sys.stdout.flush()
    with open(cache_file_name, 'rb') as input_stream:
      result = cPickle.load(input_stream)
    sys.stdout.write(' completed\n')
    sys.stdout.flush()
  else:
    print('Generating user_to_device dict')
    result = dict()
    with open('./data/UserInfo.tsv') as input_stream:
      input_stream.readline()
      counter = 1
      for line in input_stream:
        if counter % 10 ** 6 == 0:
          print(counter)
        user_id = int(line[:-1].split('\t')[0])
        user_device_id = int(line[:-1].split('\t')[3])
        result[user_id] = user_device_id
        counter += 1

    sys.stdout.write('Dumping user_to_device dict to cache...')
    sys.stdout.flush()
    with open(cache_file_name, 'wb') as output_stream:
      cPickle.dump(result, output_stream)
    sys.stdout.write(' completed\n')
    sys.stdout.flush()

  return result


def generate_search_to_agent_os_dict():
  cache_file_name = './data/search_to_agent_os_dict_cached.pkl'
  if os.path.exists(cache_file_name):
    sys.stdout.write('Loading from search_to_agent_os dict cache...')
    sys.stdout.flush()
    with open(cache_file_name, 'rb') as input_stream:
      result = cPickle.load(input_stream)
    sys.stdout.write(' completed\n')
    sys.stdout.flush()
  else:
    search_to_user_dict = generate_search_to_user_dict()
    user_to_agent_os_dict = generate_user_to_agent_os_dict()
    result = dict()
    for key in search_to_user_dict:
      if search_to_user_dict[key] != NONE:
        try:
          result[key] = user_to_agent_os_dict[search_to_user_dict[key]]
        except:
          result[key] = NONE
      else:
        result[key] = NONE
    sys.stdout.write('Dumping search_to_agent_os_dict to cache...')
    sys.stdout.flush()
    with open(cache_file_name, 'wb') as output_stream:
      cPickle.dump(result, output_stream)
    sys.stdout.write(' completed\n')
    sys.stdout.flush()

  return result


def generate_search_to_device_dict():
  cache_file_name = './data/search_to_device_dict_cached.pkl'
  if os.path.exists(cache_file_name):
    sys.stdout.write('Loading from search_to_device dict cache...')
    sys.stdout.flush()
    with open(cache_file_name, 'rb') as input_stream:
      result = cPickle.load(input_stream)
    sys.stdout.write(' completed\n')
    sys.stdout.flush()
  else:
    search_to_user_dict = generate_search_to_user_dict()
    user_to_device_dict = generate_user_to_device_dict()
    result = dict()
    for key in search_to_user_dict:
      if search_to_user_dict[key] != NONE:
        try:
          result[key] = user_to_device_dict[search_to_user_dict[key]]
        except:
          result[key] = NONE
      else:
        result[key] = NONE
    sys.stdout.write('Dumping search_to_device_dict to cache...')
    sys.stdout.flush()
    with open(cache_file_name, 'wb') as output_stream:
      cPickle.dump(result, output_stream)
    sys.stdout.write(' completed\n')
    sys.stdout.flush()

  return result


def generate_category_to_parent_dict():
  category_to_parent_dict = dict()
  with open('./data/Category.tsv') as input_stream:
    input_stream.readline()
    for line in input_stream:
      category_id = int(line[:-1].split('\t')[0])
      parent_category_id = int(line[:-1].split('\t')[2])
      category_to_parent_dict[category_id] = parent_category_id
  return category_to_parent_dict


def generate_ad_to_category_dict():
  if os.path.exists('./data/ad_to_category_dict_cached.pkl'):
    sys.stdout.write('Loading from ad_to_category_dict cache...')
    sys.stdout.flush()
    with open('./data/ad_to_category_dict_cached.pkl', 'rb') as input_stream:
      ad_to_category_dict = cPickle.load(input_stream)
    sys.stdout.write(' completed\n')
    sys.stdout.flush()
  else:
    ad_to_category_dict = dict()
    with open('./data/AdsInfo.tsv') as input_stream:
      input_stream.readline()
      for line in input_stream:
        ad_id = int(line[:-1].split('\t')[0])
        category_id_str = line[:-1].split('\t')[2]
        if category_id_str == '':
          ad_to_category_dict[ad_id] = NONE
        else:
          category_id = int(line[:-1].split('\t')[2])
          ad_to_category_dict[ad_id] = category_id
    sys.stdout.write('Dumping ad_to_category_dict to cache...')
    sys.stdout.flush()
    with open('./data/ad_to_category_dict_cached.pkl', 'wb') as output_stream:
      cPickle.dump(ad_to_category_dict, output_stream)
    sys.stdout.write(' completed\n')
    sys.stdout.flush()

  return ad_to_category_dict


def generate_search_to_parent_category_dict():
  search_to_parent_category_file_name = './data/search_to_parent_category_dict_cached.pkl'

  if os.path.exists(search_to_parent_category_file_name):
    sys.stdout.write('Loading from search_to_parent_category_dict cache...')
    sys.stdout.flush()
    with open(search_to_parent_category_file_name, 'rb') as input_stream:
      search_to_parent_category_dict = cPickle.load(input_stream)
    sys.stdout.write(' completed\n')
    sys.stdout.flush()
  else:
    search_to_category_dict = generate_search_to_category_dict()
    category_to_parent_dict = generate_category_to_parent_dict()
    search_to_parent_category_dict = dict()
    for key in search_to_category_dict:
      if search_to_category_dict[key] != NONE:
        try:
          search_to_parent_category_dict[key] = category_to_parent_dict[
                                                                      search_to_category_dict[key]]
        except:
          search_to_parent_category_dict[key] = NONE
      else:
        search_to_parent_category_dict[key] = NONE
    sys.stdout.write('Dumping search_to_parent_category_dict to cache...')
    sys.stdout.flush()
    with open(search_to_parent_category_file_name, 'wb') as output_stream:
      cPickle.dump(search_to_parent_category_dict, output_stream)
    sys.stdout.write(' completed\n')
    sys.stdout.flush()

  return search_to_parent_category_dict


def generate_last_searches_test_dict(file_name):
  assert(file_name == './data/testSearchStream.tsv')
  user_to_last_search_time_dict = dict()
  with open(file_name) as train_input_stream:
    train_input_stream.readline()
    train_line = train_input_stream.readline()
    train_search_id = int(train_line[:-1].split('\t')[1])
    with open('./data/SearchInfo.tsv') as info_input_stream:
      info_input_stream.readline()
      for info_line in info_input_stream:
        search_id = int(info_line[:-1].split('\t')[0])
        if search_id % 10 ** 6 == 0:
          print(search_id)
        search_date = info_line[:-1].split('\t')[1]
        user_id = int(info_line[:-1].split('\t')[3])
        while train_search_id < search_id:
          train_line = train_input_stream.readline()
          try:
            train_search_id = int(train_line[:-1].split('\t')[1])
          except:
            break
        while train_search_id == search_id:
          if not user_id in user_to_last_search_time_dict:
            user_to_last_search_time_dict[user_id] = search_date
          else:
            new_time = max(user_to_last_search_time_dict[user_id], search_date)
            user_to_last_search_time_dict[user_id] = new_time
          train_line = train_input_stream.readline()
          try:
            train_search_id = int(train_line[:-1].split('\t')[1])
          except:
            break

          # print(user_to_last_search_time_dict)
          # raw_input()
  return user_to_last_search_time_dict


def generate_search_to_location_dict():
  search_to_location_file_name = './data/search_to_location_dict_cached.pkl'
  if os.path.exists(search_to_location_file_name):
    sys.stdout.write('Loading from search_to_location_dict cache...')
    sys.stdout.flush()
    with open(search_to_location_file_name, 'rb') as input_stream:
      search_to_location_dict = cPickle.load(input_stream)
    sys.stdout.write(' completed\n')
    sys.stdout.flush()
  else:
    print('Generating search_to_location dict')
    search_to_location_dict = dict()
    with open('./data/SearchInfo.tsv') as input_stream:
      input_stream.readline()
      counter = 1
      for line in input_stream:
        if counter % 10 ** 6 == 0:
          print(counter)
        search_id = int(line[:-1].split('\t')[0])
        location_id_str = line[:-1].split('\t')[6]
        if location_id_str == '':
          search_to_location_dict[search_id] = NONE
        else:
          try:
            location_id = int(location_id_str)
          except:
            location_id = NONE
          search_to_location_dict[search_id] = location_id
        counter += 1
    sys.stdout.write('Dumping search_to_location_dict to cache...')
    sys.stdout.flush()
    with open(search_to_location_file_name, 'wb') as output_stream:
      cPickle.dump(search_to_location_dict, output_stream)
    sys.stdout.write(' completed\n')
    sys.stdout.flush()

  return search_to_location_dict


def generate_search_to_region_dict():
  search_to_region_file_name = './data/search_to_region_dict_cached.pkl'

  if os.path.exists(search_to_region_file_name):
    sys.stdout.write('Loading from search_to_region_dict cache...')
    sys.stdout.flush()
    with open(search_to_region_file_name, 'rb') as input_stream:
      search_to_region_dict = cPickle.load(input_stream)
    sys.stdout.write(' completed\n')
    sys.stdout.flush()
  else:
    search_to_location_dict = generate_search_to_location_dict()
    location_to_region_dict = generate_location_to_region_dict()
    search_to_region_dict = dict()
    for key in search_to_location_dict:
      if search_to_location_dict[key] != NONE:
        try:
          search_to_region_dict[key] = location_to_region_dict[search_to_location_dict[key]]
        except:
          search_to_region_dict[key] = NONE
      else:
        search_to_region_dict[key] = NONE
    sys.stdout.write('Dumping search_to_region_dict to cache...')
    sys.stdout.flush()
    with open(search_to_region_file_name, 'wb') as output_stream:
      cPickle.dump(search_to_region_dict, output_stream)
    sys.stdout.write(' completed\n')
    sys.stdout.flush()

  return search_to_region_dict


def generate_search_to_is_user_logged_on_dict():
  if os.path.exists('./data/search_to_is_user_logged_on_dict_cached.pkl'):
    sys.stdout.write('Loading from search_to_is_user_logged_on cache...')
    sys.stdout.flush()
    with open('./data/search_to_is_user_logged_on_dict_cached.pkl', 'rb') as input_stream:
      search_to_is_user_logged_on_dict = cPickle.load(input_stream)
    sys.stdout.write(' completed\n')
    sys.stdout.flush()
  else:
    search_to_is_user_logged_on_dict = dict()
    with open('./data/SearchInfo.tsv') as input_stream:
      input_stream.readline()
      for line in input_stream:
        search_id = int(line[:-1].split('\t')[0])
        is_user_logged_on = int(line[:-1].split('\t')[4])
        search_to_is_user_logged_on_dict[search_id] = is_user_logged_on
    sys.stdout.write('Dumping search_to_is_user_logged_on_dict to cache...')
    sys.stdout.flush()
    with open('./data/search_to_is_user_logged_on_dict_cached.pkl', 'wb') as output_stream:
      cPickle.dump(search_to_is_user_logged_on_dict, output_stream)
    sys.stdout.write(' completed\n')
    sys.stdout.flush()
  return search_to_is_user_logged_on_dict


def generate_ad_to_parent_category_dict():
  ad_to_category_dict = generate_ad_to_category_dict()
  category_to_parent_dict = generate_category_to_parent_dict()
  ad_to_parent_category_dict = dict()
  for key in ad_to_category_dict:
    if ad_to_category_dict[key] != NONE:
      ad_to_parent_category_dict[key] = category_to_parent_dict[ad_to_category_dict[key]]
    else:
      ad_to_parent_category_dict[key] = NONE
  return ad_to_parent_category_dict


def generate_location_to_region_dict():
  location_to_region_dict = dict()
  with open('./data/Location.tsv') as input_stream:
    input_stream.readline()
    for line in input_stream:
      location_id = int(line[:-1].split('\t')[0])
      if line[:-1].split('\t')[2] != '':
        region_id = int(line[:-1].split('\t')[2])
        location_to_region_dict[location_id] = region_id
  return location_to_region_dict


def generate_ad_to_region_dict():
  ad_to_location_dict = generate_ad_to_location_dict()
  location_to_region_dict = generate_location_to_region_dict()
  ad_to_region_dict = dict()
  for key in ad_to_location_dict:
    if ad_to_location_dict[key] != NONE:
      ad_to_region_dict[key] = location_to_region_dict[ad_to_location_dict[key]]
    else:
      ad_to_region_dict[key] = NONE
    # print(key, ad_to_location_dict[key], ad_to_region_dict[key])
    # raw_input()
  # print(ad_to_region_dict[326584])
  # sys.exit()
  return ad_to_region_dict


