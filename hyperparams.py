from models import NeighborhoodProbabilityModel
from models import NearestNeighborsModel
from wrappers import SimpleVotingRegression

NONE = -1
# BOUNDARY_SHIFT = 0.0001
BOUNDARY_SHIFT = 0.001
# NUMBER_OF_BINS = 250
NUMBER_OF_BINS = 1000
NUMBER_OF_USER_CTR_BINS = 1
MAX_PRICE = 50000
NUMBER_OF_PRICE_BINS = 5000

# 'UserAgentOSID'

# LIST_OF_FEATURES = ['Position', 'Price', 'HistCTR',
#                     'ParentCategoryID', 'SearchParentCategoryID']
# BIN_NUMBERS = [None, 5000, 1000, None, None]
# MAX_VALUES = [None, 50000, 1.0, None, None]

LISTS_OF_FEATURES = [['Position', 'Price', 'HistCTR',
                     'ParentCategoryID', 'SearchParentCategoryID'],
                     ['Position', 'Price', 'HistCTR',
                      'ParentCategoryID', 'UserAgentOSID']]
BIN_NUMBERS = [[None, 5000, 1000, None, None], [None, 5000, 1000, None, None]]
MAX_VALUES = [[None, 50000, 1.0, None, None], [None, 50000, 1.0, None, None]]
COEFFICIENTS = [0.8, 0.2]

# LIST_OF_FEATURES = ['Position', 'HistCTR', 'Price']
# 
# LEARNING_MODEL = NeighborhoodProbabilityModel
# LEARNING_MODEL = NearestNeighborsModel
LEARNING_MODEL = SimpleVotingRegression
MODE = 'sparse'
ORIGINAL_CELL_MULTIPLIER = 1
FEATURE_MULTIPLIERS = [0.8, 0.066, 0.2, 0.066, 0.2]
# FEATURE_MULTIPLIERS = [0.8, 0.066, 0.2]
# FEATURE_MULTIPLIERS = [0] * 5

# MODEL_ARGS = {}

NUMBER_OF_NEIGHBORS = 100000

# LIST_OF_VOTING_MODELS = [
# NeighborhoodProbabilityModel(...)]

LIST_OF_COEFFICIENTS = [1]

MODEL_TYPES = [NeighborhoodProbabilityModel, NeighborhoodProbabilityModel]

