import copy
from constants import NONE
from constants import LIST_OF_POSITIONS
from constants import LIST_OF_PARENT_CLASSES
from constants import LIST_OF_USER_AGENT_OS_IDS
# from hyperparams import LIST_OF_FEATURES
# from hyperparams import LEARNING_MODEL
# from hyperparams import MODE
# from hyperparams import ORIGINAL_CELL_MULTIPLIER
# from hyperparams import FEATURE_MULTIPLIERS
# from hyperparams import NUMBER_OF_NEIGHBORS
# from hyperparams import BOUNDARY_SHIFT
# from hyperparams import NUMBER_OF_PRICE_BINS
# from hyperparams import MAX_PRICE
# from hyperparams import NUMBER_OF_BINS
# from hyperparams import NUMBER_OF_USER_CTR_BINS
# from hyperparams import NUMBER_OF_PRICE_BINS
# from hyperparams import MAX_PRICE

LIST_OF_SUPPORTED_FEATURES = ['Position', 'HistCTR', 'ParentCategoryID', 'UserCTR',
                              'IsUserLoggedOn', 'SearchParentCategoryID', 'SearchRegionID',
                              'Price', 'UserAgentOSID', 'UserDeviceID'] # UserAgentFamilyID # 
# LIST_OF_BINS = range(NUMBER_OF_BINS)
LIST_OF_LOGGED_ON = [0, 1]
# LIST_OF_USER_CTR_BINS = range(NUMBER_OF_USER_CTR_BINS)
LIST_OF_REGIONS = [NONE, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
                   21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 32, 33, 34, 35, 36, 37, 38, 39, 40,
                   41, 42, 43, 44, 45, 47, 46, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60,
                   61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80,
                   81, 82, 83, 84]
# LIST_OF_PRICES = range(NUMBER_OF_PRICE_BINS)
LIST_OF_USER_DEVICE_IDS = [NONE] + range(1, 4220, 1)


def to_index(value, feature_name, number_of_bins=None, max_value=None):
  if feature_name == 'Position':
    return LIST_OF_POSITIONS.index(value)
  elif feature_name == 'HistCTR':
    # return LIST_OF_BINS.index(min(int(value * NUMBER_OF_BINS), NUMBER_OF_BINS - 1))
    return min(int(value * number_of_bins / max_value), number_of_bins - 1)
  elif feature_name == 'ParentCategoryID' or feature_name == 'SearchParentCategoryID':
    return LIST_OF_PARENT_CLASSES.index(value)
  elif feature_name == 'IsUserLoggedOn':
    return LIST_OF_LOGGED_ON.index(value)
  elif feature_name == 'UserCTR':
    return min(int(value * number_of_bins / max_value), number_of_bins - 1)
    # return LIST_OF_USER_CTR_BINS.index(min(int(value * NUMBER_OF_USER_CTR_BINS),
    #                                        NUMBER_OF_USER_CTR_BINS - 1))
  elif feature_name == 'RegionID' or feature_name == 'SearchRegionID':
    return LIST_OF_REGIONS.index(value)
  elif feature_name == 'Price':
    return min(int(value * number_of_bins / max_value), number_of_bins - 1)
    # return LIST_OF_PRICES.index(min(value * NUMBER_OF_PRICE_BINS / MAX_PRICE,
    #                                 NUMBER_OF_PRICE_BINS - 1))
  elif feature_name == 'UserAgentOSID':
    return LIST_OF_USER_AGENT_OS_IDS.index(value)
  elif feature_name == 'UserDeviceID':
    return LIST_OF_USER_DEVICE_IDS.index(value)
  else:
    raise ValueError('{} not in list of supported features'.format(feature_name))


# extracts the subsample with needed features from the sample with all features
# len(all_features) == len(sample)
def to_subsample(sample, needed_features, all_features):
  return tuple([sample[all_features.index(feature_name)] for feature_name in needed_features])


class SimpleVotingRegression(object):
  def __init__(self, list_of_models, features_lists, bin_numbers, max_values, coefficients,
               all_features):
    assert(len(list_of_models) == len(bin_numbers))
    assert(len(list_of_models) == len(max_values))
    assert(len(list_of_models) == len(coefficients))
    self.list_of_models = copy.deepcopy(list_of_models)
    self.bin_numbers = bin_numbers
    self.max_values = max_values
    self.coefficients = coefficients
    self.features_lists = features_lists
    self.all_features = all_features
  def fit(self, list_of_samples, list_of_labels):
    for model, list_of_features, number_of_bins, max_value in zip(
                      self.list_of_models, self.features_lists, self.bin_numbers, self.max_values):
      model.fit([tuple(map(lambda(x, y, z, t): to_index(x, y, z, t),
                           zip(to_subsample(sample, list_of_features, self.all_features),
                               list_of_features, number_of_bins, max_value)))
                   for sample in list_of_samples], list_of_labels)
      # model.fit(list_of_samples, list_of_labels)
  def predict(self, sample):
    prediction = 0
    for model, coefficient, list_of_features, number_of_bins, max_value in zip(
                   self.list_of_models, self.coefficients,
                   self.features_lists, self.bin_numbers, self.max_values):
      # prediction += coefficient * model.predict(sample)
      prediction += coefficient * model.predict(tuple(map(lambda(x, y, z, t): to_index(x, y, z, t),
                                     zip(to_subsample(sample, list_of_features, self.all_features),
                                         list_of_features, number_of_bins, max_value))))
    return prediction

