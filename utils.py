import sys
import os
import time
import math
import warnings
warnings.filterwarnings('ignore')
import numpy
import cPickle
import sql_utils
from generate_validation_subsets import generate_last_searches_dict
from dict_generators import generate_search_to_agent_os_dict
from dict_generators import generate_search_to_device_dict
from dict_generators import generate_ad_to_parent_category_dict
from dict_generators import generate_search_to_parent_category_dict
from models import ExactProbabilityModel
from models import NeighborhoodProbabilityModel
from constants import NONE
from constants import LIST_OF_POSITIONS
from constants import LIST_OF_PARENT_CLASSES
from constants import LIST_OF_USER_AGENT_OS_IDS

def get_list_of_values(feature_name):
  if feature_name == 'Position':
    return LIST_OF_POSITIONS
  elif feature_name == 'HistCTR':
    return LIST_OF_BINS
  elif feature_name == 'ParentCategoryID' or feature_name == 'SearchParentCategoryID':
    return LIST_OF_PARENT_CLASSES
  elif feature_name == 'IsUserLoggedOn':
    return LIST_OF_LOGGED_ON
  elif feature_name == 'UserCTR':
    return LIST_OF_USER_CTR_BINS
  elif feature_name == 'RegionID' or feature_name == 'SearchRegionID':
    return LIST_OF_REGIONS
  elif feature_name == 'Price':
    return LIST_OF_PRICES
  elif feature_name == 'UserAgentOSID':
    return LIST_OF_USER_AGENT_OS_IDS
  elif feature_name == 'UserDeviceID':
    return LIST_OF_USER_DEVICE_IDS
  else:
    raise ValueError


def get_model_shape(list_of_features, bin_numbers):
  assert(len(list_of_features) == len(bin_numbers))
  list_of_shapes = []
  for feature_name, number_of_bins in zip(list_of_features, bin_numbers):
    if number_of_bins != None:
      list_of_shapes.append(number_of_bins)
    else:
      list_of_shapes.append(len(get_list_of_values(feature_name)))
  return tuple(list_of_shapes)
  # return map(lambda(x): len(get_list_of_values(x)), list_of_features)




def to_dump_code(feature_name):
  if feature_name == 'Position':
    return 'position'
  elif feature_name == 'HistCTR':
    return 'hist_ctr'
  elif feature_name == 'ParentCategoryID':
    return 'parent_category_id'
  elif feature_name == 'IsUserLoggedOn':
    return 'is_user_logged_on'
  elif feature_name == 'UserCTR':
    return 'user_ctr'
  elif feature_name == 'RegionID':
    return 'region_id'
  elif feature_name == 'SearchParentCategoryID':
    return 'search_parent_category_id'
  elif feature_name == 'SearchRegionID':
    return 'search_region_id'
  elif feature_name == 'Price':
    return 'price'
  elif feature_name == 'UserAgentOSID':
    return 'user_agent_os_id'
  elif feature_name == 'UserDeviceID':
    return 'user_device_id'
  else:
    raise ValueError('Unsupported feature name {}'.format(feature_name))


# AdsInfo dicts

def generate_ad_to_location_dict():
  if os.path.exists('./data/ad_to_location_dict_cached.pkl'):
    sys.stdout.write('Loading from ad_to_location_dict cache...')
    sys.stdout.flush()
    with open('./data/ad_to_location_dict_cached.pkl', 'rb') as input_stream:
      ad_to_location_dict = cPickle.load(input_stream)
    sys.stdout.write(' completed\n')
    sys.stdout.flush()
  else:
    ad_to_location_dict = dict()
    with open('./data/AdsInfo.tsv') as input_stream:
      input_stream.readline()
      for line in input_stream:
        ad_id = int(line[:-1].split('\t')[0])
        location_id_str = line[:-1].split('\t')[1]
        if location_id_str == '':
          ad_to_location_dict[ad_id] = NONE
        else:
          location_id = int(location_id_str)
          ad_to_location_dict[ad_id] = location_id
    sys.stdout.write('Dumping ad_to_location_dict to cache...')
    sys.stdout.flush()
    with open('./data/ad_to_location_dict_cached.pkl', 'wb') as output_stream:
      cPickle.dump(ad_to_location_dict, output_stream)
    sys.stdout.write(' completed\n')
    sys.stdout.flush()
  return ad_to_location_dict


def generate_ad_to_price_dict():
  caching_file_name = './data/ad_to_price_dict_cached.pkl'

  if os.path.exists(caching_file_name):
    sys.stdout.write('Loading from ad_to_price_dict cache...')
    sys.stdout.flush()
    with open(caching_file_name, 'rb') as input_stream:
      ad_to_price_dict = cPickle.load(input_stream)
    sys.stdout.write(' completed\n')
    sys.stdout.flush()
  else:
    ad_to_price_dict = dict()
    with open('./data/AdsInfo.tsv') as input_stream:
      input_stream.readline()
      for line in input_stream:
        ad_id = int(line[:-1].split('\t')[0])
        price_str = line[:-1].split('\t')[4]
        if price_str == '':
          ad_to_price_dict[ad_id] = 0
        else:
          price = int(float(price_str))
          ad_to_price_dict[ad_id] = price
    sys.stdout.write('Dumping ad_to_price_dict to cache...')
    sys.stdout.flush()
    with open(caching_file_name, 'wb') as output_stream:
      cPickle.dump(ad_to_price_dict, output_stream)
    sys.stdout.write(' completed\n')
    sys.stdout.flush()
  return ad_to_price_dict


def generate_user_to_ctr_dict(bounding_search_times_dict):
  user_to_positive_clicks_dict = dict()
  user_to_negative_clicks_dict = dict()
  for key in bounding_search_times_dict:
    user_to_positive_clicks_dict[key] = 0
    user_to_negative_clicks_dict[key] = 0
  with open('./data/trainSearchStream.tsv') as train_input_stream:
    train_input_stream.readline()
    train_line = train_input_stream.readline()
    train_search_id = int(train_line[:-1].split('\t')[0])
    train_is_click = int(train_line[:-1].split('\t')[5])
    with open('./data/SearchInfo.tsv') as info_input_stream:
      info_input_stream.readline()
      for info_line in info_input_stream:
        info_search_id = int(info_line[:-1].split('\t')[0])
        if info_search_id % 10 ** 6 == 0:
          print(info_search_id)
        info_date = info_line[:-1].split('\t')[1]
        info_user_id = int(info_line[:-1].split('\t')[3])
        while train_search_id < info_search_id:
          train_line = train_input_stream.readline()
          train_search_id = int(train_line[:-1].split('\t')[0])
        while train_search_id == info_search_id:
          if (info_user_id in bounding_search_times_dict
              and bounding_search_times_dict[info_user_id] > info_date
              and train_line[:-1].split('\t')[-1] != ''):
            if train_is_click == 1:
              user_to_positive_clicks_dict[info_user_id] += 1
            elif train_is_click == 0:
              user_to_negative_clicks_dict[info_user_id] += 1
            else:
              raise ValueError
          train_line = train_input_stream.readline()
          try:
            train_search_id = int(train_line[:-1].split('\t')[0])
            train_is_click = int(train_line[:-1].split('\t')[5])
          except:
            break

  user_to_ctr_dict = dict()
  for key in user_to_positive_clicks_dict:
    if user_to_positive_clicks_dict[key] + user_to_negative_clicks_dict[key] == 0:
      user_to_ctr_dict[key] = 0
    else:
      user_to_ctr_dict[key] = (float(user_to_positive_clicks_dict[key])
                        / (user_to_positive_clicks_dict[key] + user_to_negative_clicks_dict[key]))

  return user_to_ctr_dict


def is_timestamp_cached(feature_name, timestamp):
  timestamp_modifier = 'pre_' * timestamp
  file_name = ('./data/user_to_' + timestamp_modifier + 'last_search_time_'
               + to_dump_code(feature_name) + '_cached.pkl')
  return os.path.exists(file_name)


def is_cached(feature_name):
  return is_timestamp_cached(feature_name, 2)


def is_validation_cached(feature_name):
  return is_timestamp_cached(feature_name, 1)


def is_test_cached(feature_name):
  return is_timestamp_cached(feature_name, 0)


def make_timestamp_cache(feature_name, list_of_values, timestamp):
  timestamp_modifier = 'pre_' * timestamp
  file_name = ('./data/user_to_' + timestamp_modifier + 'last_search_time_'
               + to_dump_code(feature_name) + '_cached.pkl')
  with open(file_name, 'wb') as output_stream:
    cPickle.dump(list_of_values, output_stream)


def make_cache(feature_name, list_of_values):
  make_timestamp_cache(feature_name, list_of_values, 2)


def make_validation_cache(feature_name, list_of_values):
  make_timestamp_cache(feature_name, list_of_values, 1)


def make_test_cache(feature_name, list_of_values):
  make_timestamp_cache(feature_name, list_of_values, 0)


def load_timestamp_cache(feature_name, timestamp):
  timestamp_modifier = 'pre_' * timestamp
  file_name = ('./data/user_to_' + timestamp_modifier + 'last_search_time_'
               + to_dump_code(feature_name) + '_cached.pkl')
  with open(file_name, 'rb') as input_stream:
    return cPickle.load(input_stream)


def load_timestamp_labels_cache(timestamp):
  if timestamp == 0:
    labels_cache_file_name = './data/test_ids_cached.pkl'
  else:
    labels_cache_file_name = ('./data/user_to_' + 'pre_' * timestamp
                              + 'last_search_labels_cached.pkl')
  with open(labels_cache_file_name, 'rb') as input_stream:
    list_of_labels = cPickle.load(input_stream)
  return list_of_labels


def load_cache(feature_name):
  return load_timestamp_cache(feature_name, 2)


def load_validation_cache(feature_name):
  return load_timestamp_cache(feature_name, 1)


def load_test_cache(feature_name):
  return load_timestamp_cache(feature_name, 0)


def load_user_to_search_time_dict(timestamp):
  timestamp_modifier = 'pre_' * timestamp
  user_to_search_time_cache_file_name = ('./data/user_to_' + timestamp_modifier
                                         + 'last_search_time_dict_cached.pkl')
  if os.path.exists(user_to_search_time_cache_file_name):
    sys.stdout.write('Loading from user_to_last_search_time_dict cache...')
    sys.stdout.flush()
    with open(user_to_search_time_cache_file_name, 'rb') as input_stream:
      user_to_last_search_time_dict = cPickle.load(input_stream)
    sys.stdout.write(' completed\n')
    sys.stdout.flush()
  else:
    user_to_last_search_time_dict = generate_last_searches_dict(last_search_finding_file_name)
    sys.stdout.write('Dumping user_to_last_search_time_dict cache...')
    sys.stdout.flush()
    with open(user_to_search_time_cache_file_name, 'wb') as output_stream:
      cPickle.dump(user_to_last_search_time_dict, output_stream)
    sys.stdout.write(' completed\n')
    sys.stdout.flush()
  return user_to_last_search_time_dict


def load_timestamp_feature(feature_name, timestamp):
  if feature_name == 'SearchRegionID':
    search_to_region = generate_search_to_region_dict()
  elif feature_name == 'Price':
    ad_to_price = generate_ad_to_price_dict()
  elif feature_name == 'UserAgentOSID':
    search_to_agent_os = generate_search_to_agent_os_dict()
  elif feature_name == 'UserDeviceID':
    search_to_device = generate_search_to_device_dict()

  user_to_search_time_dict = load_user_to_search_time_dict(timestamp)

  if timestamp == 0:
    search_stream_file_name = './data/testSearchStream.tsv'
    stream_search_id_index = 1
    stream_ad_id_index = 2
    if feature_name != 'Price' and feature_name != 'UserAgentOSID':
      raise NotImplementedError('Feature name {} is not supported for loading test data'
                                .format(feature_name))
  else:
    search_stream_file_name = './data/trainSearchStream.tsv'
    stream_search_id_index = 0
    stream_ad_id_index = 1

  timestamp_feature_values = []
  with open(search_stream_file_name) as train_input_stream:
    train_input_stream.readline()
    train_line = train_input_stream.readline()
    train_search_id = int(train_line[:-1].split('\t')[stream_search_id_index])
    with open('./data/SearchInfo.tsv') as info_input_stream:
      info_input_stream.readline()
      for info_line in info_input_stream:
        info_search_id = int(info_line[:-1].split('\t')[0])
        if info_search_id % 10 ** 6 == 0:
          print(info_search_id)
        info_date = info_line[:-1].split('\t')[1]
        info_user_id = int(info_line[:-1].split('\t')[3])
        info_is_user_logged_on = int(info_line[:-1].split('\t')[4])
        while train_search_id < info_search_id:
          train_line = train_input_stream.readline()
          try:
            train_search_id = int(train_line[:-1].split('\t')[stream_search_id_index])
          except:
            break
        while train_search_id == info_search_id:
          if (train_line[:-1].split('\t')[-1] != ''
              and ((info_user_id in user_to_search_time_dict
                    and user_to_search_time_dict[info_user_id] == info_date) or timestamp == 0)):
            if feature_name == 'Position':
              position = int(train_line[:-1].split('\t')[2])
              timestamp_feature_values.append(position)
            elif feature_name == 'HistCTR':
              hist_ctr = float(train_line[:-1].split('\t')[4])
              timestamp_feature_values.append(hist_ctr)
            elif feature_name == 'ParentCategoryID':
              ad_id = int(train_line[:-1].split('\t')[1])
              parent_category_id = ad_to_parent_category_dict[ad_id]
              timestamp_feature_values.append(parent_category_id)
            elif feature_name == 'IsUserLoggedOn':
              timestamp_feature_values.append(info_is_user_logged_on)
            elif feature_name == 'UserCTR':
              if info_user_id in user_to_ctr_dict:
                user_ctr = user_to_ctr_dict[info_user_id]
              else:
                user_ctr = 0
              timestamp_feature_values.append(user_ctr)
            elif feature_name == 'RegionID':
              raise ValueError
              ad_id = int(train_line[:-1].split('\t')[1])
              region_id = ad_to_region_dict[ad_id]
              timestamp_feature_values.append(region_id)
            elif feature_name == 'SearchParentCategoryID':
              search_id = int(train_line[:-1].split('\t')[stream_search_id_index])
              parent_category_id = search_to_parent_category_dict[search_id]
              timestamp_feature_values.append(parent_category_id)
            elif feature_name == 'SearchRegionID':
              search_id = int(train_line[:-1].split('\t')[stream_search_id_index])
              region_id = search_to_region[search_id]
              timestamp_feature_values.append(region_id)
            elif feature_name == 'Price':
              ad_id = int(train_line[:-1].split('\t')[stream_ad_id_index])
              price = ad_to_price[ad_id]
              timestamp_feature_values.append(price)
            elif feature_name == 'UserAgentOSID':
              search_id = int(train_line[:-1].split('\t')[stream_search_id_index])
              agent_os_id = search_to_agent_os[search_id]
              timestamp_feature_values.append(agent_os_id)
            elif feature_name == 'UserDeviceID':
              search_id = int(train_line[:-1].split('\t')[stream_search_id_index])
              device_id = search_to_device[search_id]
              timestamp_feature_values.append(device_id)
            else:
              raise ValueError
            # print(feature_values)
            # raw_input()
          else:
            pass
          train_line = train_input_stream.readline()
          try:
            train_search_id = int(train_line[:-1].split('\t')[stream_search_id_index])
          except:
            break
 
  return timestamp_feature_values


def load_timestamp_labels(timestamp):
  raise NotImplementedError
  pass


def load_timestamp_data(list_of_features, timestamp):
  assert(timestamp in [0, 1, 2]) # Only validation and train datasets supported yet
  """
      Now it loads features from the list of last searches in ./data/validation/train.csv file
      So the are pre-last searches from the original train file and pre-pre-last searches from
      the whole dataset
  """

  for feature_name in list_of_features:
    if not is_timestamp_cached(feature_name, timestamp):
      timestamp_feature_values = load_timestamp_feature(feature_name, timestamp)
      print('New {} values array length: {}'.format(feature_name, len(timestamp_feature_values)))
      make_timestamp_cache(feature_name, timestamp_feature_values, timestamp)

  if timestamp == 0:
    labels_cache_file_name = './data/test_ids_cached.pkl'
  else:
    labels_cache_file_name = ('./data/user_to_' + 'pre_' * timestamp
                              + 'last_search_labels_cached.pkl')
    if not os.path.exists(labels_cache_file_name):
      timestamp_labels = load_timestamp_labels(timestamp)
      with open(labels_cache_file_name, 'wb') as output_stream:
        cPickle.dump(timestamp_labels, output_stream)

  # Loading from cache
  if (all([is_timestamp_cached(feature_name, timestamp) for feature_name in list_of_features])
      and os.path.exists(labels_cache_file_name)):
    list_of_samples = zip(*[load_timestamp_cache(feature_name, timestamp)
                            for feature_name in list_of_features])
    with open(labels_cache_file_name, 'rb') as input_stream:
      list_of_labels = cPickle.load(input_stream)
  else:
    raise AssertionError('WTF is going on?')

  # if not os.path.exists(labels_cache_file_name):
  #   with open(labels_cache_file_name, 'wb') as output_stream:
  #     cPickle.dump(list_of_labels, output_stream)

  return list_of_samples, list_of_labels


def load_train_data(list_of_features):
  return load_timestamp_data(list_of_features, 2)


def load_validation_data(list_of_features):
  return load_timestamp_data(list_of_features, 1)


def load_test_data(list_of_features):
  return load_timestamp_data(list_of_features, 0)
  # Loading from cache
  if (all([is_test_cached(feature_name) for feature_name in list_of_features])
      and os.path.exists('./data/test_ids_cached.pkl')):
    list_of_samples = zip(*[load_test_cache(feature_name)
                            for feature_name in list_of_features])
    with open('./data/test_ids_cached.pkl', 'rb') as input_stream:
      list_of_test_ids = cPickle.load(input_stream)

  else:
    if os.path.exists('./data/user_to_last_search_time_dict_cached.pkl'):
      sys.stdout.write('Loading from user_to_last_search_time_dict cache...')
      sys.stdout.flush()
      with open('./data/user_to_last_search_time_dict_cached.pkl', 'rb') as input_stream:
        user_to_last_search_time_dict = cPickle.load(input_stream)
      sys.stdout.write(' completed\n')
      sys.stdout.flush()
    else:
      # user_to_last_search_time_dict = generate_last_searches_dict('./data/testSearchStream.tsv')
      user_to_last_search_time_dict = generate_last_searches_test_dict(
                                                                    './data/testSearchStream.tsv')
      sys.stdout.write('Dumping user_to_last_search_time_dict cache...')
      sys.stdout.flush()
      with open('./data/user_to_last_search_time_dict_cached.pkl', 'wb') as output_stream:
        cPickle.dump(user_to_last_search_time_dict, output_stream)
      sys.stdout.write(' completed\n')
      sys.stdout.flush()

    ad_to_parent_category_dict = generate_ad_to_parent_category_dict()

    if os.path.exists('./data/user_to_last_search_time_user_to_ctr_dict_cached.pkl'):
      sys.stdout.write('Loading from user_to_ctr_dict cache...')
      sys.stdout.flush()
      with open('./data/user_to_last_search_time_user_to_ctr_dict_cached.pkl',
                'rb') as input_stream:
        user_to_ctr_dict = cPickle.load(input_stream)
      sys.stdout.write(' completed\n')
      sys.stdout.flush()
    else:
      user_to_ctr_dict = generate_user_to_ctr_dict(user_to_last_search_time_dict)
      sys.stdout.write('Dumping user_to_ctr_dict cache...')
      sys.stdout.flush()
      with open('./data/user_to_last_search_time_user_to_ctr_dict_cached.pkl',
                'wb') as output_stream:
        cPickle.dump(user_to_ctr_dict, output_stream)
      sys.stdout.write(' completed\n')
      sys.stdout.flush()

    if 'SearchParentCategoryID' in list_of_features:
      search_to_parent_category_dict = generate_search_to_parent_category_dict()
    if 'Price' in list_of_features:
      ad_to_price = generate_ad_to_price_dict()

    list_of_samples = []
    list_of_test_ids = []
    with open('./data/testSearchStream.tsv') as train_input_stream:
      train_input_stream.readline()
      train_line = train_input_stream.readline()
      test_id = int(train_line[:-1].split('\t')[0])
      train_search_id = int(train_line[:-1].split('\t')[1])
      with open('./data/SearchInfo.tsv') as info_input_stream:
        info_input_stream.readline()
        for info_line in info_input_stream:
          info_search_id = int(info_line[:-1].split('\t')[0])
          if info_search_id % 10 ** 6 == 0:
            print(info_search_id)
          info_date = info_line[:-1].split('\t')[1]
          info_user_id = int(info_line[:-1].split('\t')[3])
          info_is_user_logged_on = int(info_line[:-1].split('\t')[4])
          while train_search_id < info_search_id:
            train_line = train_input_stream.readline()
            try:
              test_id = int(train_line[:-1].split('\t')[0])
              train_search_id = int(train_line[:-1].split('\t')[1])
            except:
              break
          while train_search_id == info_search_id:
            if train_line[:-1].split('\t')[-1] != '':
              feature_values = []
              for feature in list_of_features:
                ad_id = int(train_line[:-1].split('\t')[2])
                if feature == 'Position':
                  position = int(train_line[:-1].split('\t')[3])
                  feature_values.append(position)
                elif feature == 'HistCTR':
                  hist_ctr = float(train_line[:-1].split('\t')[5])
                  feature_values.append(hist_ctr)
                elif feature == 'ParentCategoryID':
                  ad_id = int(train_line[:-1].split('\t')[2])
                  parent_category_id = ad_to_parent_category_dict[ad_id]
                  feature_values.append(parent_category_id)
                elif feature == 'IsUserLoggedOn':
                  feature_values.append(info_is_user_logged_on)
                elif feature == 'UserCTR':
                  if info_user_id in user_to_ctr_dict:
                    user_ctr = user_to_ctr_dict[info_user_id]
                  else:
                    user_ctr = 0
                  feature_values.append(user_ctr)
                elif feature == 'SearchParentCategoryID':
                  search_id = int(train_line[:-1].split('\t')[1])
                  parent_category_id = search_to_parent_category_dict[search_id]
                  feature_values.append(parent_category_id)
                elif feature_name == 'Price':
                  price = ad_to_price[ad_id]
                  feature_values.append(price)
                else:
                  raise ValueError
              list_of_samples.append(feature_values)
              list_of_test_ids.append(test_id)
              # print(feature_values)
              # raw_input()
            else:
              pass
            train_line = tra.shapein_input_stream.readline()
            try:
              test_id = int(train_line[:-1].split('\t')[0])
              train_search_id = int(train_line[:-1].split('\t')[1])
            except:
              break

  for feature_counter, feature_name in enumerate(list_of_features):
    if not is_test_cached(feature_name):
      sys.stdout.write('Caching test ' + str(feature_name) + '...')
      sys.stdout.flush()
      make_test_cache(feature_name, [sample[feature_counter] for sample in list_of_samples])
      sys.stdout.write(' completed\n')
      sys.stdout.flush()

  if not os.path.exists('./data/test_ids_cached.pkl'):
    with open('./data/test_ids_cached.pkl', 'wb') as output_stream:
      cPickle.dump(list_of_test_ids, output_stream)

  return list_of_samples, list_of_test_ids


def to_multiplier_likelyhood(sample, multiplier):
  position, hist_ctr, parent_category = sample
  if position == 1:
    likelyhood = multiplier * hist_ctr
  else:
    likelyhood = hist_ctr
  # if parent_category == 6:
  #   likelyhood *= 1.0
  return likelyhood


# if __name__ == '__main__':
#   time_start = time.time()
#   print(LEARNING_MODEL.__name__, ORIGINAL_CELL_MULTIPLIER)
#   print(LIST_OF_FEATURES)
#   print(FEATURE_MULTIPLIERS)
#   print('Boundary shift: {}'.format(BOUNDARY_SHIFT))
#   if 'HistCTR' in LIST_OF_FEATURES:
#     print('HistCTR bins: {}'.format(NUMBER_OF_BINS))
#   if 'Price' in LIST_OF_FEATURES:
#     print('Price bins: {}, Price max: {}'.format(NUMBER_OF_PRICE_BINS, MAX_PRICE))
# 
#   train_samples, train_labels = load_train_data(LIST_OF_FEATURES)
# 
#   model_shape = map(lambda(x): len(get_list_of_values(x)), LIST_OF_FEATURES)
# 
#   if LEARNING_MODEL.__name__ == 'NeighborhoodProbabilityModel':
#     MODEL_ARGS = {'model_shape': model_shape, 'boundary_threshold': BOUNDARY_SHIFT,
#                   'neighborhood_radius': None, 'mode': 'sparse',
#                   'original_cell_multiplier': ORIGINAL_CELL_MULTIPLIER,
#                   'feature_multipliers': FEATURE_MULTIPLIERS}
#   elif LEARNING_MODEL.__name__ == 'NearestNeighborsModel':
#     MODEL_ARGS = {'model_shape': model_shape, 'boundary_threshold': BOUNDARY_SHIFT,
#                   'number_of_neighbors': NUMBER_OF_NEIGHBORS,
#                   'feature_distances': range(1, len(model_shape) + 1, 1)}
#   else:
#     raise ValueError
# 
# 
#   print(MODEL_ARGS)
# 
#   train_model = SimpleVotingRegression([LEARNING_MODEL(**MODEL_ARGS)], [1])
# 
#   train_model.fit([tuple(map(lambda(x, y): to_index(x, y), zip(sample, LIST_OF_FEATURES)))
#                    for sample in train_samples], train_labels)
# 
#   validation_samples, validation_labels = load_validation_data(LIST_OF_FEATURES)
# 
#   total_loss = 0
#   samples_counter = 0
#   for sample, label in zip(validation_samples, validation_labels):
#     prediction = train_model.predict(tuple(map(lambda(x, y): to_index(x, y),
#                                            zip(sample, LIST_OF_FEATURES))))
#     total_loss += label * math.log(prediction) + (1 - label) * math.log(1 - prediction)
#     if samples_counter % (len(validation_samples) / 100) == 0:
#       sys.stdout.write('\r')
#       sys.stdout.write(str(samples_counter / (len(validation_samples) / 100)))
#       sys.stdout.flush()
#     samples_counter += 1
#     # print(sample)
#     # print(len(train_model.ctr))
#     # print(len(train_model.positive_clicks))
#     # print(len(train_model.negative_clicks))
#   sys.stdout.write('\r')
#   sys.stdout.flush()
#   print('--- ', -total_loss / len(validation_samples), ' ---')
# 
#   print('Total validation time: {}'.format(time.time() - time_start))
#   sys.exit()
#   print('Predicting started')
#   test_model = LEARNING_MODEL(model_shape, BOUNDARY_SHIFT, None, MODE,
#                               ORIGINAL_CELL_MULTIPLIER, FEATURE_MULTIPLIERS)
#   test_model.fit([tuple(map(lambda(x, y): to_index(x, y), zip(sample, LIST_OF_FEATURES)))
#                    for sample in validation_samples], validation_labels)
# 
#   test_samples, test_ids = load_test_data(LIST_OF_FEATURES)
# 
#   predictions = []
#   samples_counter = 0
#   for sample_id, sample in zip(test_ids, test_samples):
#     prediction = test_model.predict(tuple(map(lambda(x, y): to_index(x, y),
#                                            zip(sample, LIST_OF_FEATURES))))
#     predictions.append((sample_id, prediction))
#     samples_counter += 1
#     if samples_counter % 100000 == 0:
#       sys.stdout.write('\r')
#       sys.stdout.write(str(samples_counter))
#       sys.stdout.flush()
# 
#   # sys.exit()
#   write_predictions(predictions)

