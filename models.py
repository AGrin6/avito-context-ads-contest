import sys
import numpy
from constants import X


class ExactProbabilityModel(object):
  def __init__(self, list_of_features, boundary_threshold):
    self.ctr = None
    self.list_of_features = list_of_features
    self.boundary_threshold = boundary_threshold
  def fit(self, list_of_samples, list_of_labels):
    positive_clicks = numpy.zeros(map(lambda(x): len(get_list_of_values(x)), self.list_of_features),
                                  dtype=numpy.uint32)
    negative_clicks = numpy.zeros(map(lambda(x): len(get_list_of_values(x)), self.list_of_features),
                                  dtype=numpy.uint32)
    for sample, label in zip(list_of_samples, list_of_labels):
      if label == 0:
        negative_clicks[tuple(map(lambda(x, y): to_index(x, y),
                        zip(sample, self.list_of_features)))] += 1
      else:
        positive_clicks[tuple(map(lambda(x, y): to_index(x, y),
                        zip(sample, self.list_of_features)))] += 1

    self.ctr = numpy.divide(numpy.array(positive_clicks, dtype=float),
                            (positive_clicks + negative_clicks))

  def predict(self, sample):
    prediction = self.ctr[tuple(map(lambda(x, y): to_index(x, y),
                           zip(sample, self.list_of_features)))]
    return min(max(self.boundary_threshold, prediction), 1 - self.boundary_threshold)


def get_neighborhood_array(numpy_array):
  return sum([numpy.concatenate(numpy_array.shape[i]
                             * [numpy.expand_dims(numpy.sum(numpy_array, axis=i), axis=i)], axis=i) 
                                for i in xrange(len(numpy_array.shape))])


class NeighborhoodProbabilityModel(object):
  def __init__(self, model_shape, boundary_threshold, neighborhood_radius, mode,
               original_cell_multiplier, feature_multipliers):
    assert(len(feature_multipliers) == len(model_shape))
    self.ctr = None
    self.boundary_threshold = boundary_threshold
    self.mode = mode
    self.original_cell_multiplier = original_cell_multiplier
    self.number_of_dims = len(model_shape)
    self.feature_multipliers = feature_multipliers
    if self.mode == 'sparse':
      self.ctr = dict()
      self.positive_clicks = dict()
      self.negative_clicks = dict()
      self.sparse_array_shape = model_shape
      self.positive_click_projections = []
      self.negative_click_projections = []
      for i in xrange(len(model_shape)):
        self.positive_click_projections.append(dict())
        self.negative_click_projections.append(dict())

  def fit(self, list_of_samples, list_of_labels):
    assert(len(list_of_samples) == len(list_of_labels))
    if self.mode == 'dense':
      positive_clicks = numpy.zeros(map(lambda(x): len(get_list_of_values(x)),
                                        self.list_of_features),
                                    dtype=numpy.uint32)
      negative_clicks = numpy.zeros(map(lambda(x): len(get_list_of_values(x)),
                                        self.list_of_features),
                                    dtype=numpy.uint32)
      for sample, label in zip(list_of_samples, list_of_labels):
        if label == 0:
          negative_clicks[sample] += 1
        else:
          positive_clicks[sample] += 1

      positive_values = (get_neighborhood_array(positive_clicks)
                         + self.original_cell_multiplier * positive_clicks)
      negative_values = (get_neighborhood_array(negative_clicks)
                         + self.original_cell_multiplier * negative_clicks)

      self.ctr = numpy.divide(numpy.array(positive_values, dtype=float),
                              (positive_values + negative_values))

    elif self.mode == 'sparse':
      for sample, label in zip(list_of_samples, list_of_labels):
        if label == 0:
          if sample in self.negative_clicks:
            self.negative_clicks[sample] += 1
          else:
            self.negative_clicks[sample] = 1
        else:
          if sample in self.positive_clicks:
            self.positive_clicks[sample] += 1
          else:
            self.positive_clicks[sample] = 1

    else:
      raise ValueError('Wrong mode. Only "dense" and "sparse" allowed')

  def predict(self, sample):
    if self.mode == 'dense':
      prediction = self.ctr[sample]
    elif self.mode == 'sparse':
      # 0.02 = N / 0
      # 0.01 = 0 / 0
      sample_coordinates = sample
      if not sample_coordinates in self.ctr:
        total_positive_clicks = 0
        total_negative_clicks = 0
        for i in xrange(self.number_of_dims):
          projection_coordinates = sample_coordinates[:i] + sample_coordinates[i + 1:]
          if not projection_coordinates in self.positive_click_projections[i]:
            positive_projection_value = 0
            negative_projection_value = 0
            for j in xrange(self.sparse_array_shape[i]): # can optimize here
              sparse_array_coordinates = sample_coordinates[:i] + (j,) + sample_coordinates[i + 1:]
              if sparse_array_coordinates in self.positive_clicks:
                positive_projection_value += self.positive_clicks[sparse_array_coordinates]
              if sparse_array_coordinates in self.negative_clicks:
                negative_projection_value += self.negative_clicks[sparse_array_coordinates]
            self.positive_click_projections[i][projection_coordinates] = positive_projection_value
            self.negative_click_projections[i][projection_coordinates] = negative_projection_value
          total_positive_clicks += (self.positive_click_projections[i][projection_coordinates]
                                    * self.feature_multipliers[i])
          total_negative_clicks += (self.negative_click_projections[i][projection_coordinates]
                                    * self.feature_multipliers[i])
        if sample_coordinates in self.positive_clicks:
          total_positive_clicks += (self.original_cell_multiplier
                                    * self.positive_clicks[sample_coordinates])
        if sample_coordinates in self.negative_clicks:
          total_negative_clicks += (self.original_cell_multiplier
                                    * self.negative_clicks[sample_coordinates])
        if total_positive_clicks + total_negative_clicks != 0:
          self.ctr[sample_coordinates] = (float(total_positive_clicks)
                                                 / (total_positive_clicks + total_negative_clicks))
        else:
          self.ctr[sample_coordinates] = 0.01
      prediction = self.ctr[sample_coordinates]
    else:
      raise ValueError('Wrong mode. Only "dense" and "sparse" allowed')

    return min(max(self.boundary_threshold, prediction), 1 - self.boundary_threshold)


# returns value of the new subarray and adds it and all supporting subarrays to precomputed dict
def process_subarray(subarray_code, precomputed_dict, model_shape):
  if subarray_code in precomputed_dict:
    return precomputed_dict[subarray_code]

  for i in xrange(len(subarray_code)):
    if subarray_code[i] == X:
      result = 0
      for j in xrange(model_shape[i]):
        new_subarray_code = subarray_code[:i] + (j,) + subarray_code[i + 1:]
        result += process_subarray(new_subarray_code, precomputed_dict, model_shape)
      precomputed_dict[subarray_code] = result
      return result

  precomputed_dict[subarray_code] = 0
  return 0


class NearestNeighborsModel(object):
  def __init__(self, model_shape, boundary_threshold, number_of_neighbors, feature_distances):
    assert(len(feature_distances) == len(model_shape))
    self.ctr = dict()
    self.model_shape = model_shape
    self.boundary_threshold = boundary_threshold
    self.number_of_neighbors = number_of_neighbors
    self.feature_distances = feature_distances
    # for both cell and projections
    self.positive_clicks = dict()
    self.negative_clicks = dict()

  def fit(self, list_of_samples, list_of_labels):
    assert(len(list_of_samples) == len(list_of_labels))

    for sample, label in zip(list_of_samples, list_of_labels):
      if label == 0:
        if sample in self.negative_clicks:
          self.negative_clicks[sample] += 1
        else:
          self.negative_clicks[sample] = 1
      else:
        if sample in self.positive_clicks:
          self.positive_clicks[sample] += 1
        else:
          self.positive_clicks[sample] = 1

  def predict(self, sample):
    if not sample in self.ctr:
      total_positive_clicks = 0
      total_negative_clicks = 0
      visited_neighbors = 0
      if sample in self.positive_clicks:
        total_positive_clicks += self.positive_clicks[sample]
        visited_neighbors += self.positive_clicks[sample]
      if sample in self.negative_clicks:
        total_negative_clicks += self.negative_clicks[sample]
        visited_neighbors += self.negative_clicks[sample]

      axis_expansion = [False] * len(self.model_shape)
      # while visited_neighbors < self.number_of_neighbors:
      while axis_expansion != [True] * len(self.model_shape):
        # print(axis_expansion, visited_neighbors)
        expanding_axis_index = None
        for i in xrange(len(self.feature_distances)):
          if (not axis_expansion[i]
              and (expanding_axis_index == None
                   or self.feature_distances[i] < self.feature_distances[expanding_axis_index])):
            expanding_axis_index = i
        axis_expansion[expanding_axis_index] = True
        # print(expanding_axis_index)
        # raw_input()

        # axis_expansion_codes = []
        # axis_counter = 0
        # for is_axis_expanded in axis_expansion:
        #   if is_axis_expanded or axis_counter == expanding_axis_index:
        #     axis_expansion_codes.append(X)
        #   else:
        #     axis_expansion_codes.append(sample[axis_counter])
        #   axis_counter += 1

        axis_expansion_codes = (sample[0:expanding_axis_index] + (X,)
                                + sample[expanding_axis_index +1:])

        neighborhood_code = tuple(axis_expansion_codes)
        new_positive_clicks = process_subarray(neighborhood_code, self.positive_clicks,
                                               self.model_shape)
        total_positive_clicks = new_positive_clicks
        new_negative_clicks = process_subarray(neighborhood_code, self.negative_clicks,
                                                  self.model_shape)
        total_negative_clicks = new_negative_clicks
        visited_neighbors = new_positive_clicks + new_negative_clicks

      if total_positive_clicks + total_negative_clicks != 0:
        self.ctr[sample] = (float(total_positive_clicks)
                                                 / (total_positive_clicks + total_negative_clicks))
      else:
        self.ctr[sample] = 0.01
    prediction = self.ctr[sample]
    return min(max(self.boundary_threshold, prediction), 1 - self.boundary_threshold)


class LinearRegressionModel(object):
  def __init__(self, list_of_coefficients, boundary_threshold):
    self.coefficients = list_of_coefficients
    self.boundary_threshold = boundary_threshold
  def fit(self, list_of_samples, list_of_labels):
    pass
  def predict(sample):
    pass

